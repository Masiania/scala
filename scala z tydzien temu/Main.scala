def remElems[A](seq: Seq[A], k: Int): Seq[A] ={
  seq
    .zipWithIndex
    .filter(x => x match{
      case (co, i) if i==k => false
      case _ => true
    })
    .map(_(0))
    //.map(cos => cos(0))
}

def diff[A](seq1: Seq[A], seq2: Seq[A]): Seq[A] ={
  seq1.zip(seq2)
  .filter(o => o match{
    case (a, b) if a==b => false
    case _ => true
  })
  .map(_(0))
}

def sumOption(seq: Seq[Option[Double]]): Double = {
  seq
    .foldLeft(0.0)((acc, el) => el match{
      case None => acc
      case Some(x) => acc+x
    })
}

def deStutter[A](seq: Seq[A]): Seq[A] = {
  seq
    .foldLeft(Seq(): Seq[A])((acc, el2) => acc match{
      case Seq() => Seq(el2)
      case accumul:+zmienna if zmienna==el2 => acc
      case accumul:+zmienna if zmienna!=el2 => acc:+el2
    })
}

def isOrdered[A](seq: Seq[A])(leq:(A, A) => Boolean): Boolean = {
  seq
    .sliding(2)
    .foldLeft(true)((acc, elem3) => acc match{
      case false => false
      case true => leq(elem3(0),elem3(1)) 
    })
}


def costam1(a: Int, p: Int): Boolean = {
  a<p
}

def costam2(a: Int, p: Int): Boolean = {
  a==p
}

//def freq[A](seq: Seq[A]): Seq[(A, Int)] = {
//  seq
//    .groupBy(identity)
//    .map(x => (x(0), x.size))
//    .toList
//}

def threeNumbers(n: Int): Seq[(Int, Int, Int)] = {
  for {
    a <- (1 to n)
    b <- (1 to n)
    c <- (1 to n)
    if a*a + b*b == c*c
    if a < b
  } yield (a, b, c)
}

@main
def main: Unit = {
  println(remElems(Seq(1,4,8,9), 2))
  println(diff(Seq(1,3,6), Seq(1, 2, 5, 8)))
  println(sumOption(Seq(Some(1.0), None, Some(1.0), Some(3.0), None)))
  println(deStutter(Seq(1,1,4,4,6,7,8,8)))
  println(isOrdered(Seq(1,4,6))(costam1))
  println(isOrdered(Seq(1,1,4,6,6,0))(costam2))
  println(isOrdered(Seq(1,1,1,1))(costam2))
  //println(freq(Seq('a','b','a','c','c','a')))
  println(threeNumbers(30))
}





