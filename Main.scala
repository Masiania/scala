
@main
def main: Unit = {
  println(obramuj_tekst("corstam\nnoijeszczecos\nooooooooooooooo"))
}
def obramuj_tekst(text: String): String = {
  val textArray: Array[String] = text.split("\n")
  val maxLength: Int = textArray.maxBy(_.length).length
  val formatRow = (row: String) => {"* " + row + (" " * (maxLength - row.length)) + " *"}
  "*" * (maxLength + 4) + "\n" + textArray.map(r => formatRow(r)).mkString("\n") + "\n" + "*" * (maxLength + 4)
}



