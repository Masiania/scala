import nltk
nltk.download('punkt')
nltk.download('stopwords')
nltk.download('wordnet')
nltk.download('omw-1.4')
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer
import collections
import matplotlib.pyplot as plt
from wordcloud import WordCloud


with open('ex1.txt') as f:
    text=f.read()

text = word_tokenize(text)

stop_words = stopwords.words('english')
stop_words.extend([".", "''", ",", "'s", "``", "'", "'ll", "'d", "n't"])
text = [w for w in text if not w.lower() in set(stop_words)]
print('after filtering stop-words: ', len(text))

word_lemmatizer = WordNetLemmatizer()
text = [word_lemmatizer.lemmatize(w) for w in text]
print('after lemmatization: ', len(text))

counts = collections.Counter(text).most_common(10)
plt.bar(list(map(lambda x: x[0], counts)), list(map(lambda x: x[1], counts)))
plt.show()

wordcloud = WordCloud(max_font_size=50, max_words=100, background_color="white").generate(' '.join(text))
plt.figure()
plt.imshow(wordcloud, interpolation="bilinear")
plt.axis("off")
plt.show()
