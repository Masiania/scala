import pyswarms as ps
from pyswarms.backend.topology import Pyramid
from pyswarms.utils.functions import single_obj as fx
from pyswarms.utils.plotters.plotters import plot_contour
from pyswarms.utils.plotters.formatters import Mesher

options = {'c1': 0.5, 'c2': 0.3, 'w': 0.9}

#Zad 1

my_topology = Pyramid(static=False)

optimizer = ps.single.GlobalBestPSO(n_particles=10, dimensions=2, options=options)
optimizer2 = ps.single.GeneralOptimizerPSO(n_particles=10, dimensions=2, options=options, topology=my_topology)

stats = optimizer.optimize(fx.sphere, iters=100)
stats2 = optimizer2.optimize(fx.sphere, iters=100)

cost_history = optimizer.cost_history
cost_history2 = optimizer2.cost_history

#plot_cost_history(cost_history)
#plot_cost_history(cost_history2)
#plt.show()

#Zad 2

m = Mesher(func=fx.sphere)
animation = plot_contour(pos_history=optimizer.pos_history,
mesher=m,
mark=(0, 0))
animation.save('plot0.gif', writer='imagemagick', fps=10)

#Zad 3

optimizer3 = ps.single.GlobalBestPSO(n_particles=10, dimensions=2, options=options)
optimizer4 = ps.single.GlobalBestPSO(n_particles=10, dimensions=2, options=options)
optimizer5 = ps.single.GlobalBestPSO(n_particles=10, dimensions=2, options=options)

stats3 = optimizer3.optimize(fx.sphere, iters=100)
stats4 = optimizer4.optimize(fx.eggholder, iters=100)
stats5 = optimizer5.optimize(fx.ackley, iters=100)

cost_history3 = optimizer3.cost_history
cost_history4 = optimizer4.cost_history
cost_history5 = optimizer5.cost_history

#plot_cost_history(cost_history3)
#plot_cost_history(cost_history4)
#plot_cost_history(cost_history5)
#plt.show()

