def divide[A](list: List[A]): (List[A], List[A]) = {
	@tailrec
	def helper(l: List[A], acc1: List[A], acc2[A], counter: Int): (List[A], List[A]) = {
		l match {
			case List() => (acc1, acc2)
			case l::t if counter % 2 == 0 => helper(t, acc1::l, acc2, counter+1)
			case l::t => helper(t, acc1, acc2::l, counter+1)
		} 
	}
	helper(list, List(), List(), 0)
}


def compress[A](list: List[A]): List[(A, Int)] = {
	@tailrec
	def helper(l: List[A], acc: List[(A, Int)], counter: Int): List[(A, Int)] = {
		l match {
			case List() => acc
			case h1::h2::l if h1 == h2 => helper(t, acc, counter + 1)
			case h1::h2::l if h2 == None => helper(t, acc, counter + 1)
			case h1::h2::l => helper(t, acc::(temp, counter), 0)
		}
	}
	helper(list, List(), 0)
}

def lab03_compress[A](list: List[A]): List[(A, Int)] = {
  @tailrec
  def helper(lst: List[A], acc: List[(A, Int)], counter: Int): List[(A, Int)] = {
    lst match {
      case List() => acc
      case e1::List() => (e1, counter)::acc
      case e1::e2::tail if e1 == e2 => helper(e2::tail, acc, counter + 1)
      case e1::e2::tail => helper(e2::tail, (e1, counter)::acc, 1)
    }
  }

  helper(list, List(), 1).reverse
}

def merge[A](a: List[A], b: List[A])(leq: (A, A) => Boolean): List[A] = {
  @tailrec
  def helper(lst_a: List[A], lst_b: List[A], acc: List[A]): List[A] = {
    (lst_a, lst_b) match {
      case (List(), List()) => acc
      case (List(), head::tail) => helper(List(), tail, head::acc)
      case (head::tail, List()) => helper(tail, List(), head::acc)
      case (headA::tailA, headB::tailB) => {
        leq(headA, headB) match {
          case true => helper(tailA, headB::tailB, headA::acc)
          case false => helper(headA::tailA, tailB, headB::acc)
        }
      }
    }
  }

  helper(a, b, List()).reverse
}

def checkSub[A](l: List[A], lSub: List[A]): Boolean = {
  @tailrec
  def lSubContains(el: A, lst: List[A]): Boolean = {
    lst match {
      case List() => false
      case head::tail if head == el => true
      case head::tail => lSubContains(el, tail) 
    }
  }

  @tailrec
  def helper(lst: List[A]): Boolean = {
    lst match {
      case List() => true
      case head::tail => lSubContains(head, lSub) match {
        case true => helper(tail)
        case false => false
      }
    }
  }

  helper(l)
}
