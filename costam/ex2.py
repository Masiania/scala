import nltk
nltk.download('subjectivity')
nltk.download('vader_lexicon')
from nltk.classify import NaiveBayesClassifier
from nltk.corpus import subjectivity
from nltk.sentiment import SentimentAnalyzer, SentimentIntensityAnalyzer
from nltk.sentiment.util import *
import text2emotion as te


n_instances = 100
subj_docs = [(sent, 'subj') for sent in subjectivity.sents(categories='subj')[:n_instances]]
obj_docs = [(sent, 'obj') for sent in subjectivity.sents(categories='obj')[:n_instances]]

train_subj_docs = subj_docs[:80]
test_subj_docs = subj_docs[80:100]
train_obj_docs = obj_docs[:80]
test_obj_docs = obj_docs[80:100]
training_docs = train_subj_docs+train_obj_docs
testing_docs = test_subj_docs+test_obj_docs

sentim_analyzer = SentimentAnalyzer()
all_words_neg = sentim_analyzer.all_words([mark_negation(doc) for doc in training_docs])

unigram_feats = sentim_analyzer.unigram_word_feats(all_words_neg, min_freq=4)
sentim_analyzer.add_feat_extractor(extract_unigram_feats, unigrams=unigram_feats)

training_set = sentim_analyzer.apply_features(training_docs)
test_set = sentim_analyzer.apply_features(testing_docs)

trainer = NaiveBayesClassifier.train
classifier = sentim_analyzer.train(trainer, training_set)

positive_sentence = 'Very quiet and very appealing. Totally summer like and camping mode. All is neat, nice, clean and very well taken care of. Customer experience from the moment of arrival to the check-out is extremely good. Friendly staff just add to that positive experience. We only wish we could have stayed there longer.'
negative_sentence = 'Booked a night in Rome and the hotels’ driver stranded us at airport AND the next day when we had to get to pier for cruise. We had confirmed with him multiple times. No management on site. We asked manager to get us a taxi and threatened bad review. 30 minutes later, Booking.com wrote asking why we were a “no show!” Immediately replied and told them I had text messages to PROVE hotel was lying to ward off a bad review! 25-30 emails, disputes and phone calls, we are nowhere. Half the emails bounced back as “undeliverable” and despite submitting cold, hard proof we DID stay there via screenshots, they continue to ask us for our departure invoice despite being fully aware the hotel won’t give us one because it would be an admission they are lying!'

for sentence in [positive_sentence, negative_sentence]:
    sid = SentimentIntensityAnalyzer()
    print(sentence)
    ss = sid.polarity_scores(sentence)
    for k in sorted(ss):
        print('{0}: {1}, '.format(k, ss[k]), end='')
    print()
    print('text2emotion: ', te.get_emotion(sentence))
    print('\n')
