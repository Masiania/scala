//zad1
def countChars(str: String): Int = {
  str
  .distinct
  .knownSize
}

def countCharstest(str: String): String = {
  str
  .distinct
}
//zad2
val strefy: Seq[String] = java.util.TimeZone.getAvailableIDs.toSeq

def strefyfunk(seq: Seq[String]): Seq[String] = {
  seq
  .filter(x => x match{
    case (cos) if cos contains "Europe/" => true
    case _ => false

  })
  .map(_.stripPrefix("Europe/"))
  .sortBy(_.size)
}
@main
def kolekcje: Unit = {
  println(countCharstest(String("aabbctthh")))
  println(countChars(String("aabbctthh")))
  println(strefyfunk(strefy))
}
