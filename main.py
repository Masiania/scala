import math
import random
import pandas as pd
import csv
import matplotlib
import matplotlib.pyplot as plt

list1 = [3, 8, 9, 10, 12]
list2 = [8, 7, 7, 5, 6]
#a, b, c
iloczyn_list = []
suma = []
skalarny = 0
euklid = 0

for i in range(len(list1)):
    iloczyn_list.append(list1[i]*list2[i])
    suma.append(list1[i]+list2[i])
    skalarny = skalarny+(list1[i]*list2[i])
    euklid = euklid+((list1[i]-list2[i])**2)
euklid = math.sqrt(euklid)
print(iloczyn_list, " Iloczyn")
print(suma, "Suma")
print(skalarny, "Skalarne")
print(euklid, "Liczba euklidowa")
#d
lista_randomowa = []
for i in range (50):
    lista_randomowa.append(random.randrange(1,100))

print(lista_randomowa, "Lista wygenerowana", len(lista_randomowa))
#e
srednia_randomowej = 0
min_randomowej = 0
max_randomowej = 0
odczylenie_randomowej = 0

for i in range (len(lista_randomowa)):
    srednia_randomowej = srednia_randomowej + lista_randomowa[i]
    min_randomowej = min(lista_randomowa)
    max_randomowej = max(lista_randomowa)
srednia_randomowej=srednia_randomowej/(len(lista_randomowa))
for i in range (len(lista_randomowa)):
    odczylenie_randomowej = odczylenie_randomowej+((lista_randomowa[i]-srednia_randomowej)**2)
odczylenie_randomowej = math.sqrt(odczylenie_randomowej/len(lista_randomowa))
print(srednia_randomowej, "Srednia listy")
print(min_randomowej, "Minimalna", max_randomowej, "Maxymalna")
print(odczylenie_randomowej, "Odchylenie standardowe")
#f
lista_normalizowana = []
pozycja_max = 0
liczba_na_pozycji_max = 0
for i in range(len(lista_randomowa)):
    lista_normalizowana.append((lista_randomowa[i]-min_randomowej)/(max_randomowej-min_randomowej))
for i in range(len(lista_normalizowana)):
    lista_normalizowana[i] = round(lista_normalizowana[i],3)
print(lista_normalizowana, "Lista Normalizowana")
pozycja_max = lista_randomowa.index(max_randomowej)
liczba_na_pozycji_max = lista_normalizowana[pozycja_max]
print((pozycja_max, "Pozycja Maksymalnej"))
print(liczba_na_pozycji_max, "Nowa liczba maksymalna")
#g
lista_standardyzowana = []
for i in range(len(lista_randomowa)):
    lista_standardyzowana.append((lista_randomowa[i]-srednia_randomowej)/odczylenie_randomowej)
for i in range(len(lista_standardyzowana)):
    lista_standardyzowana[i] = round(lista_standardyzowana[i],3)
print(lista_standardyzowana, "Lista Standardyzowana")
srednia_standardyzowanej = 0
odchylenie_standardowej = 0
for i in range (len(lista_standardyzowana)):
    srednia_standardyzowanej = srednia_standardyzowanej + lista_standardyzowana[i]
    odchylenie_standardowej = odchylenie_standardowej + ((lista_standardyzowana[i] - srednia_standardyzowanej) ** 2)
srednia_standardyzowanej=srednia_standardyzowanej/(len(lista_standardyzowana))
odchylenie_standardowej = math.sqrt(odchylenie_standardowej/len(lista_standardyzowana))
print(srednia_standardyzowanej, "Srednia Standaryzowanej")
print(odchylenie_standardowej, "Odchylenie standardyzowanej")

#zadanie 3
#a


with open('miasta.csv', 'r', encoding='utf-8') as csvfile:
    csvreader = csv.DictReader(csvfile)

    for row in csvreader:
        print(row)

df = pd.read_csv('miasta.csv')

x=df['Rok']
y=df['Gdansk']
y2=df['Poznan']
y3=df['Szczecin']


plt.title("Lundosc Gdanska", fontdict={'fontname': 'monospace', 'fontsize': 18})
plt.xlabel('Rok')
plt.ylabel('Ilosc ludzi')
#plt.plot(x,y)
#plt.plot(x,y2)
#plt.plot(x,y3)
plt.plot(df['Gdansk'], 'r-', label='Gdansk')
plt.plot(df['Poznan'], 'y-', label='Poznan')
plt.plot(df['Szczecin'], 'b-', label='Szczecin')
plt.legend()
plt.show()

#d

